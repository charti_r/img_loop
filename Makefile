CFLAGS	+= -W -Wall -pedantic -O3
CFLAGS	+= -I./include -I./libmyX/include

CC	= gcc
RM	= rm -rf

NAME	= img_loop

SRC	= \
	src/main.cpp

OBJ	= $(SRC:.cpp=.o)

LIB	= -L ./libmyX -lmyX -lX11

all: $(NAME)

$(NAME): $(OBJ)
	$(CC) $(OBJ) $(LIB) -o $(NAME)

clean:
	$(RM) $(OBJ)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
