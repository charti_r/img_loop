#ifndef PARAM_H_
# define PARAM_H_

# include "myX.h"

typedef struct	_param
{
  myXData	*XData;
  myXWin	*XWin;
  myXImg	*XImg;
}		param;

#endif /* !PARAM_H_ */
