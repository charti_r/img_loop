#include <stdio.h>
#include <unistd.h>

#include "myX.h"

#include "const.h"
#include "param.h"

static void	reset_color_pixel(param *param, int x, int y)
{
  myXDrawPixelImg(param->XImg, x, y, 0);
  myXImgToWin(param->XData, param->XImg, param->XWin, 0, 0);
  usleep(1); /* dont crash Xserver */
  myXDisplayWin(param->XData, param->XWin);
}

static unsigned long	add_color_pixel(param *param, int x, int y)
{
  unsigned long		color;

  color = myXGetPixel(param->XImg, x, y) + 1;
  myXDrawPixelImg(param->XImg, x, y, color);
  myXImgToWin(param->XData, param->XImg, param->XWin, 0, 0);
  usleep(1); /* dont crash Xserver */
  myXDisplayWin(param->XData, param->XWin);
  return (color);
}

static void	loop(param *param)
{
  int		x, y;

  y = -1;
  while (++y < HT)
    {
      x = -1;
      while (++x < WD)
	{
	  while (add_color_pixel(param, x, y) < COLOR_MAX)
	    {
	      if (y)
		y -= 1;
	      else if (x)
		x -= 1;
	    }
	  reset_color_pixel(param, x, y);
	}
    }
}

int	main(void)
{
  param	param;

  /* Init */
  if ((param.XData = myXInit(NULL)) == NULL)
    return (1);
  if ((param.XWin = myXNewWin(param.XData, NULL, 0, 0, WD, HT, WIN_NAME)) == NULL)
    return (1);
  if ((param.XImg = myXNewImg(param.XData, WD, HT)) == NULL)
    return (1);

  myXClearWin(param.XData, param.XWin);
  myXClearImg(param.XImg);

  /* Run */
  myXDisplayWin(param.XData, param.XWin);
  loop(&param);

  /* Free */
  myXDestroyImg(param.XData, param.XImg);
  myXDestroyWin(param.XData, param.XWin);
  myXDestroyData(param.XData);

  return (0);
}
